import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dashboard43Component } from './dashboard43.component';

describe('Dashboard43Component', () => {
  let component: Dashboard43Component;
  let fixture: ComponentFixture<Dashboard43Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dashboard43Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dashboard43Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
