import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  db1 = [
    {
      Planned: 10,
      Actual: 25
    },
    {
      Planned: 250,
      Actual: 150
    },
    {
      Planned: 350,
      Actual: 340
    },
    {
      Planned: 554,
      Actual: 450
    },
    {
      Planned: 250,
      Actual: 156
    },
    {
      Planned: 10,
      Actual: 20
    },
    {
      Planned: 250,
      Actual: 260
    },
    {
      Planned: 750,
      Actual: 550
    },
    {
      Planned: 250,
      Actual: 150
    },
    {
      Planned: 350,
      Actual: 250
    },
    {
      Planned: 756,
      Actual: 535
    },
    {
      Planned: 850,
      Actual: 630
    }
  ];

  db2 = [
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    }
  ];

  db41 = [
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    }
  ];

  db42 = [
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    }
  ];

  db43 = [
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {

      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    }
  ];

  db44 = [
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    },
    {
      P_profit: 55,
      P_expenses: 20,
      P_salary: 30,
      F_profit: 55,
      F_expenses: 20,
      F_salary: 30
    }
  ];

  db5 = [
    {

      sold: 150
    },
    {

      sold: 250
    },
    {

      sold: 350
    },
    {

      sold: 554
    },
    {

      sold: 250
    },
    {

      sold: 150
    },
    {

      sold: 250
    },
    {

      sold: 750
    },
    {

      sold: 250
    },
    {

      sold: 350
    },
    {

      sold: 756
    },
    {

      sold: 850
    }
  ];

  pies = {
    pie1: [
      {
        title: 'Projektų rengimas',
        value: 10
      },
      {
        title: 'Projektų administravimas',
        value: 10
      },
      {
        title: 'Tyrimai',
        value: 10
      },
      {
        title: 'Mokymai',
        value: 10
      }
    ],
    pie2: [
      {
        title: 'Atviras',
        value: 10
      },
      {
        title: 'Supaprastntas',
        value: 10
      },
      {
        title: 'MV konkursas',
        value: 10
      }
    ],
    pie3: [
      {
        title: 'Pardavėjas 1',
        value: 10
      },
      {
        title: 'Pardavėjas 2',
        value: 10
      },
      {
        title: 'Pardavėjas 3',
        value: 10
      },
      {
        title: 'Pardavėjas 4',
        value: 10
      }
    ]
  };

  constructor(private http: HttpClient) { }

  months: String[] = [
    'Sausis',
    'Vasaris',
    'Kovas',
    'Balandis',
    'Gegužė',
    'Birželis',
    'Liepa',
    'Rugpjūtis',
    'Rugsėjis',
    'Spalis',
    'Lapkritis',
    'Gruodis'
  ];

  getData(url): Observable<any> {
    return this.http.get(url);
  }
}
