import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dashboard1Component } from '../dashboard1/dashboard1.component';
import { Dashboard2Component } from '../dashboard2/dashboard2.component';
import { Dashboard3Component } from '../dashboard3/dashboard3.component';
import { Dashboard4Component } from '../dashboard4/dashboard4.component';
import { Dashboard42Component } from '../dashboard42/dashboard42.component';
import { Dashboard43Component } from '../dashboard43/dashboard43.component';
import { Dashboard44Component } from '../dashboard44/dashboard44.component';
import { Dashboard5Component } from '../dashboard5/dashboard5.component';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.time) {
        this.time = params.time;
      }
    });
    this.time *= 1000;
  }

  components = [
    Dashboard1Component,
    Dashboard2Component,
    Dashboard3Component,
    Dashboard4Component,
    Dashboard42Component,
    Dashboard43Component,
    Dashboard44Component,
    Dashboard5Component
  ];
  current = 0;
  time = 15;

  ngOnInit() {
    setInterval(function () {
      if (this.current++ === this.components.length - 1) {
        this.current = 0;
      }
    }.bind(this), this.time);
  }

}
