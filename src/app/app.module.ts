import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DataService } from './data.service';
import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { AppRoutingModule } from './/app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { Dashboard3Component } from './dashboard3/dashboard3.component';
import { Dashboard4Component } from './dashboard4/dashboard4.component';
import { Dashboard5Component } from './dashboard5/dashboard5.component';
import { Dashboard42Component } from './dashboard42/dashboard42.component';
import { Dashboard43Component } from './dashboard43/dashboard43.component';
import { Dashboard44Component } from './dashboard44/dashboard44.component';
import { CarouselComponent } from './carousel/carousel.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    Dashboard1Component,
    PageNotFoundComponent,
    NavigationComponent,
    Dashboard2Component,
    Dashboard3Component,
    Dashboard4Component,
    Dashboard5Component,
    Dashboard42Component,
    Dashboard43Component,
    Dashboard44Component,
    CarouselComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [
    DataService
  ],
  entryComponents: [
    Dashboard1Component,
    Dashboard2Component,
    Dashboard3Component,
    Dashboard4Component,
    Dashboard42Component,
    Dashboard43Component,
    Dashboard44Component
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
