import { Component, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-dashboard2',
  templateUrl: './dashboard2.component.html',
  styleUrls: ['./dashboard2.component.css']
})
export class Dashboard2Component implements AfterViewInit {
  constructor(private dataService: DataService) { }
  chart = [];

  @ViewChild('graphCanvas', { read: ElementRef }) graphCanvas: ElementRef;

  ngAfterViewInit() {
    const instMonths = this.dataService.months;
    const instCanvas = this.graphCanvas;
    const instData = this.dataService.db2;
    this.chart = drawChart(instMonths, instData, instCanvas);

    function drawChart(labels, data, canvas): Chart {
      const months = labels.map(month => month.toUpperCase());
      const planProfit = data.map(dataUnit => dataUnit.P_profit);
      const planExpenses = data.map(dataUnit => dataUnit.P_expenses);
      const planSalary = data.map(dataUnit => dataUnit.P_salary);
      const factProfit = data.map(dataUnit => dataUnit.F_profit);
      const factExpenses = data.map(dataUnit => dataUnit.F_expenses);
      const factSalary = data.map(dataUnit => dataUnit.F_salary);

      const ctx = canvas.nativeElement.getContext('2d');

      return new Chart(ctx, {
        type: 'bar',
        data: {
          labels: months,
          datasets: [
            {
              label: 'Planas pajamų',
              backgroundColor: '#d14035',
              hoverBackgroundColor: '#d14035',
              stack: '0',
              data: planProfit,
              datalabels: {
                color: '#d14035',
              }
            },
            {
              label: 'Planas darbo užmokesčio',
              backgroundColor: '#72598e',
              hoverBackgroundColor: '#72598e',
              stack: '1',
              data: planSalary,
              datalabels: {
                color: '#72598e',
              }
            },
            {
              label: 'Planas išlaidų',
              backgroundColor: '#a9bb30',
              hoverBackgroundColor: '#a9bb30',
              stack: '1',
              data: planExpenses,
              datalabels: {
                align: 'end',
                anchor: 'end',
                color: '#a9bb30',
              }
            },
            {
              label: 'Faktas pajamų',
              backgroundColor: '#05a6a6',
              hoverBackgroundColor: '#05a6a6',
              stack: '2',
              data: factProfit,
              datalabels: {
                color: '#05a6a6',
              }
            },
            {
              label: 'Faktas darbo užmokesčio',
              backgroundColor: '#c2718c',
              hoverBackgroundColor: '#c2718c',
              stack: '3',
              data: factSalary,
              datalabels: {
                color: '#c2718c',
              }
            },
            {
              label: 'Faktas išlaidų',
              backgroundColor: '#eb8a3c',
              hoverBackgroundColor: '#eb8a3c',
              stack: '3',
              data: factExpenses,
              datalabels: {
                align: 'end',
                anchor: 'end',
                color: '#eb8a3c',
              }
            }
          ]
        },
        options: {
          plugins: {
            datalabels: {
              display: true,
              color: '#ccc',
              align: 'start',
              anchor: 'start',
              rotation: 270,
              formatter: function (value, context) {
                return value !== 0 ? value : null;
              }
            }
          },
          legend: {
            display: false
          },
          scales: {
            yAxes: [{
              gridLines: {
                display: false,
              },
              ticks: {
                padding: 32,
                beginAtZero: true,
                fontFamily: '\'Montserrat\', sans-serif',
                fontSize: 24,
                fontColor: '#000'
              }
            }],
            xAxes: [{
              barPercentage: 0.9,
              gridLines: {
                display: false,
              },
              ticks: {
                padding: 100,
                minRotation: 90,
                beginAtZero: true,
                fontFamily: '\'Montserrat\', sans-serif',
                fontSize: 24,
                fontColor: '#000'
              }
            }]
          }
        }
      });
    }
  }

}
