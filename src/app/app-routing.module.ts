import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { Dashboard2Component } from './dashboard2/dashboard2.component';
import { Dashboard3Component } from './dashboard3/dashboard3.component';
import { Dashboard4Component } from './dashboard4/dashboard4.component';
import { Dashboard5Component } from './dashboard5/dashboard5.component';
import { Dashboard42Component } from './dashboard42/dashboard42.component';
import { Dashboard43Component } from './dashboard43/dashboard43.component';
import { Dashboard44Component } from './dashboard44/dashboard44.component';
import { CarouselComponent } from './carousel/carousel.component';
const routes: Routes = [
  { path: '', component: NavigationComponent },
  { path: 'carousel', component: CarouselComponent },
  { path: 'db1', component: Dashboard1Component },
  { path: 'db2', component: Dashboard2Component },
  { path: 'db3', component: Dashboard3Component },
  { path: 'db4/1', component: Dashboard4Component },
  { path: 'db4/2', component: Dashboard42Component },
  { path: 'db4/3', component: Dashboard43Component },
  { path: 'db4/4', component: Dashboard44Component },
  { path: 'db4', redirectTo: '/db4/1' },
  { path: 'db5', component: Dashboard5Component },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
