import { Component, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-dashboard5',
  templateUrl: './dashboard5.component.html',
  styleUrls: ['./dashboard5.component.css']
})
export class Dashboard5Component implements AfterViewInit {

  @ViewChild('graphCanvas', { read: ElementRef }) graphCanvas: ElementRef;
  @ViewChild('pieCanvas1', { read: ElementRef }) pieCanvas1: ElementRef;
  @ViewChild('pieCanvas2', { read: ElementRef }) pieCanvas2: ElementRef;
  @ViewChild('pieCanvas3', { read: ElementRef }) pieCanvas3: ElementRef;

  chart = [];

  pieChart1 = [];
  pie1Titles = [];
  pie1Data = [];

  pieChart2 = [];
  pie2Titles = [];
  pie2Data = [];

  pieChart3 = [];
  pie3Titles = [];
  pie3Data = [];

  constructor(private dataService: DataService) { }

  ngAfterViewInit() {
    const months = this.dataService.months.map(month => month.toUpperCase());
    const sold = this.dataService.db5.map(dataUnit => dataUnit.sold);
    const ctx = this.graphCanvas.nativeElement.getContext('2d');

    const blueGradient = ctx.createLinearGradient(0, 0, 0, 400);
    blueGradient.addColorStop(0, '#1d8eba');
    blueGradient.addColorStop(1, '#0e6ea0');
    this.chart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: months,
        datasets: [
          {
            label: 'PLANAS',
            backgroundColor: blueGradient,
            hoverBackgroundColor: '#1d8eba',
            data: sold,
            datalabels: {
              align: 'start',
              anchor: 'end',
              font: {
                size: '18',
                weight: '700'
              }
            }
          },
        ]
      },
      options: {
        plugins: {
          datalabels: {
            color: '#ffc600',
            formatter: function (value, context) {
              return value !== 0 ? value + 'K€' : null;
            }
          }
        },
        legend: {
          display: false,
          position: 'right',
          labels: {
            fontFamily: '\'Montserrat\', sans-serif',
            fontSize: 25,
            fontColor: '#000'
          }
        },
        scales: {
          yAxes: [{
            scaleLabel: {
              display: false,
              fontFamily: '\'Montserrat\', sans-serif',
              fontSize: 14,
              fontColor: '#000'
            },
            gridLines: {
              color: 'rgba(0, 0, 0, 0)'
            },
            ticks: {
              beginAtZero: true,
              display: false
            }
          }],
          xAxes: [{
            barPercentage: 1.0,
            gridLines: {
              color: 'rgba(0, 0, 0, 0)'
            },
            ticks: {
              padding: 10,
              minRotation: 90,
              beginAtZero: true,
              fontFamily: '\'Montserrat\', sans-serif',
              fontSize: 14,
              fontColor: '#000'
            }
          }]
        }
      }
    });

    const pie1 = this.pieCanvas1.nativeElement.getContext('2d');
    this.pie1Titles = this.dataService.pies.pie1.map(data => data.title);
    this.pie1Data = this.dataService.pies.pie1.map(data => data.value);
    this.pieChart1 = new Chart(pie1, {
      type: 'doughnut',
      data: {
        datasets: [{
          data: this.pie1Data,
          backgroundColor: [
            '#05a6a6',
            '#d14035',
            '#a9bb30',
            '#e6873c'
          ]
        }],
        labels: this.pie1Titles
      },
      options: {
        cutoutPercentage: 30,
        plugins: {
          datalabels: {
            display: false,
            color: '#ffc600',
            formatter: function (value, context) {
              return value + 'K€';
            }
          }
        },
        legend: {
          display: false,
          position: 'right',
          labels: {
            fontFamily: '\'Montserrat\', sans-serif',
            fontSize: 25,
            fontColor: '#000'
          }
        }
      }
    });

    const pie2 = this.pieCanvas2.nativeElement.getContext('2d');
    this.pie2Titles = this.dataService.pies.pie2.map(data => data.title);
    this.pie2Data = this.dataService.pies.pie2.map(data => data.value);
    this.pieChart2 = new Chart(pie2, {
      type: 'doughnut',
      data: {
        datasets: [{
          data: this.pie2Data,
          backgroundColor: [
            '#1782ad',
            '#ca7630',
            '#ab55ba'
          ]
        }],
        labels: this.pie2Titles
      },
      options: {
        cutoutPercentage: 30,
        plugins: {
          datalabels: {
            display: false,
            color: '#ffc600',
            formatter: function (value, context) {
              return value + 'K€';
            }
          }
        },
        legend: {
          display: false,
          position: 'right',
          labels: {
            fontFamily: '\'Montserrat\', sans-serif',
            fontSize: 25,
            fontColor: '#000'
          }
        }
      }
    });

    const pie3 = this.pieCanvas3.nativeElement.getContext('2d');
    this.pie3Titles = this.dataService.pies.pie3.map(data => data.title);
    this.pie3Data = this.dataService.pies.pie3.map(data => data.value);
    this.pieChart3 = new Chart(pie3, {
      type: 'doughnut',
      data: {
        datasets: [{
          data: this.pie3Data,
          backgroundColor: [
            '#c2718c',
            '#6ba21b',
            '#72598e',
            '#3545bd'
          ]
        }],
        labels: this.pie3Titles
      },
      options: {
        cutoutPercentage: 30,
        plugins: {
          datalabels: {
            display: false,
            color: '#ffc600',
            formatter: function (value, context) {
              return value + 'K€';
            }
          }
        },
        legend: {
          display: false,
          position: 'right',
          labels: {
            fontFamily: '\'Montserrat\', sans-serif',
            fontSize: 25,
            fontColor: '#000'
          }
        }
      }
    });
  }

}
