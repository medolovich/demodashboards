import { Component, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-dashboard1',
  templateUrl: './dashboard1.component.html',
  styleUrls: ['./dashboard1.component.css']
})

export class Dashboard1Component implements AfterViewInit {
  constructor(private dataService: DataService) { }
  planned: number;
  inProgress: number;
  closed: number;
  chart = [];

  @ViewChild('graphCanvas', { read: ElementRef }) graphCanvas: ElementRef;

  ngAfterViewInit() {
    const instData = this.dataService.db1;
    const instMonths = this.dataService.months;
    const instGraphCanvas = this.graphCanvas;

    this.chart = drawChart(instMonths, instData, instGraphCanvas);
    this.planned = 5;
    this.inProgress = 2;
    this.closed = 13;

    function drawChart(labels, data, canvas): Chart {
      const months = labels.map(month => month.toUpperCase());
      const planned = data.map(dataUnit => dataUnit.Planned);
      const inFact = data.map(dataUnit => dataUnit.Actual);

      const ctx = canvas.nativeElement.getContext('2d');

      const blueGradient = ctx.createLinearGradient(0, 0, 0, 400);
      blueGradient.addColorStop(0, '#1d8eba');
      blueGradient.addColorStop(1, '#0e6ea0');

      const purpleGradient = ctx.createLinearGradient(0, 0, 0, 400);
      purpleGradient.addColorStop(0, '#bf7bad');
      purpleGradient.addColorStop(1, '#a85b93');

      return new Chart(ctx, {
        type: 'bar',
        responsive: false,
        data: {
          labels: months,
          datasets: [
            {
              label: 'PLANAS',
              backgroundColor: blueGradient,
              hoverBackgroundColor: '#1d8eba',
              data: planned,
              datalabels: {
                color: blueGradient,
              }
            },
            {
              label: 'FAKTAS',
              backgroundColor: purpleGradient,
              hoverBackgroundColor: '#bf7bad',
              data: inFact,
              datalabels: {
                color: purpleGradient,
              }
            }
          ]
        },
        options: {
          maintainAspectRatio: false,
          plugins: {
            datalabels: {
              align: 'start',
              anchor: 'start',
              font: {
                size: '18',
                weight: '700'
              },
              rotation: 270,
              formatter: function (value, context) {
                return value !== 0 ? value : null;
              }
            }
          },
          legend: {
            display: true,
            position: 'right',
            labels: {
              fontFamily: '\'Montserrat\', sans-serif',
              fontSize: 25,
              fontColor: '#000'
            }
          },
          scales: {
            yAxes: [{
              scaleLabel: {
                display: false,
                fontFamily: '\'Montserrat\', sans-serif',
                fontSize: 24,
                fontColor: '#000'
              },
              gridLines: {
                color: 'rgba(0, 0, 0, 0)'
              },
              ticks: {
                beginAtZero: true,
                display: false
              }
            }],
            xAxes: [{
              barPercentage: 1.0,
              gridLines: {
                color: 'rgba(0, 0, 0, 0)'
              },
              ticks: {
                padding: 150,
                minRotation: 90,
                beginAtZero: true,
                fontFamily: '\'Montserrat\', sans-serif',
                fontSize: 24,
                fontColor: '#000'
              }
            }]
          }
        }
      });
    }
  }

}

