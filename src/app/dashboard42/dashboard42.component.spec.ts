import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dashboard42Component } from './dashboard42.component';

describe('Dashboard42Component', () => {
  let component: Dashboard42Component;
  let fixture: ComponentFixture<Dashboard42Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dashboard42Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dashboard42Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
