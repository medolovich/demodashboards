import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dashboard44Component } from './dashboard44.component';

describe('Dashboard44Component', () => {
  let component: Dashboard44Component;
  let fixture: ComponentFixture<Dashboard44Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dashboard44Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dashboard44Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
