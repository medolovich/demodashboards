const fs = require('fs');
const {
  exec
} = require('child_process');

const PORT = process.env.PORT || 8080;
let json = {
  port: PORT,
  server: {
    "baseDir": "./dist"
  },
  open: false
};
json = JSON.stringify(json);

fs.writeFile('bs-config.json', json, 'utf8', () => console.log('success'));

exec('lite-server');
